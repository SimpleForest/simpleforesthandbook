\subsection{QSM Spherefollowingbasic}\index{QSM Spherefollowingbasic}
\label{QSM Spherefollowingbasic}

\subsubsection{Screenshots}

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.48\textwidth}
		\includegraphics[width=\textwidth]{Pictures/qsm/sphereFollowing/cloud.png}
		\caption{The input points stored in a single cloud.}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.48\textwidth}
		\includegraphics[width=\textwidth]{Pictures/qsm/sphereFollowing/qsm.png}
		\caption{The QSM without postprocessing improvements.}
	\end{minipage}
\end{figure}


\subsubsection{Step Placement}

\begin{itemize}
	\item 3D geometry
	      \begin{itemize}
		      \item QSM
	      \end{itemize}
\end{itemize}

\subsubsection{IO}

\begin{stepInput}
	Input cloud - A denoised point cloud of a tree.
\end{stepInput}
\begin{stepOutput}
	QSM Cylinder - The QSM stored in Computree format.
\end{stepOutput}
\begin{stepOutput}
	SphereFollowing QSM - An internal QSM structure used to import into other SimpleForest steps.
\end{stepOutput}
\begin{stepOutput}
	SphereFollowing parameters - The optimized sphereFollowing parameters.
\end{stepOutput}

\subsubsection{Description}

The method is used to fit cylinders into a de-noised point cloud and to build the tree model.  Spheres are utilized to follow the branching structure of the tree from the root to its tips \cite{hackenberg2014_key, hackenberg2015_key, simpletree_key}.

The cloud is downscaled with the voxel grid downscale
routine from the PCL library \cite{pcl_key}. The \textbf{voxel size} of the downscale routine is a user parameter. Additionally only the largest cluster of an euclidean clustering routine with \textbf{clustering range} to be set will be processed. This improves the parameter optimization, as non reachable points will not effect the cloud to model distance.

The algorithm:

A sphere with a center point on a skeleton axis cuts the point cloud. All points within a distance of \textbf{sphere Epsilon} to the sphere-surface are considered to be used to detect the next sphere and are put into a sub point cloud $P_{sub}$.

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.32\textwidth}
		\includegraphics[width=\textwidth]{Pictures/qsm/sphereFollowing/sphere1.jpg}
		\caption{$P_{sub}$ on the sphere surface.}
	\end{minipage}
\end{figure}


$P_{sub}$ is clustered then with \textbf{euclidean clustering distance} into $i$ clusters $P_i$, sorted by their number of points decreasingly. Each cluster represents a cross-sectional area of the stem/branch.

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.32\textwidth}
		\includegraphics[width=\textwidth]{Pictures/qsm/sphereFollowing/sphere_splita.png}
		\caption{The first cluster.}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.32\textwidth}
		\includegraphics[width=\textwidth]{Pictures/qsm/sphereFollowing/sphere_splitb.png}
		\caption{The second cluster.}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.32\textwidth}
		\includegraphics[width=\textwidth]{Pictures/qsm/sphereFollowing/sphere_splitc.png}
		\caption{The third cluster.}
	\end{minipage}
\end{figure}

A circle is fitted with one of the following methods
\begin{itemize}
	\item \textbf{Center of Mass with median distance radius}\cite{hackenberg2014_key}
	\item \textbf{Gauss Newton Least Squares}\cite{hackenberg2014_key}
	\item \textbf{Random Sample Consensus (RANSAC)} \cite{ransac_key}
	\item \textbf{Maximum Likelihood Estimator SAmple Consensus (MLESAC)} \cite{mlesac_key}
	\item \textbf{M-estimator SAmple Consensus (MSAC)} \cite{mlesac_key}
	\item \textbf{Randomized M-estimator SAmple Consensus (RMSAC)} \cite{mlesac_key}
	\item \textbf{Progressive Sample Consensus (PROSAC)} \cite{prosac_key}
	\item \textbf{Randomized RAndom SAmple Consensus (RRANSAC)} \cite{rransac_key}
	\item \textbf{Least Median of Squares (LMEDS)} \cite{lmeds_key}
\end{itemize}

into $P_i$, if the number of points in $P_i$ exceeds \textbf{minPts}. The \textbf{inlier distance} for the method is set as a user parameter as well as the number if \textbf{iterations} of this routine.

The center point of the circle, the center point of the sphere and the circle radius are chosen as cylinder parameters.

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.32\textwidth}
		\includegraphics[width=\textwidth]{Pictures/qsm/sphereFollowing/sphere2.jpg}
		\caption{The detected cylinder.}
	\end{minipage}
\end{figure}

The circle is enlarged with \textbf{sphere multiplier} and transformed to a three dimensional sphere, but the sphere radius is never allowed to be smaller than \textbf{min global radius}.

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.32\textwidth}
		\includegraphics[width=\textwidth]{Pictures/qsm/sphereFollowing/sphere3.jpg}
		\caption{The next search sphere.}
	\end{minipage}
\end{figure}

The procedure is repeated recursively until no more cross sectional areas can be found.

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.32\textwidth}
		\includegraphics[width=\textwidth]{Pictures/qsm/sphereFollowing/branch_order_1.png}
		\caption{The first part of the tree is modelled.}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.32\textwidth}
		\includegraphics[width=\textwidth]{Pictures/qsm/sphereFollowing/branch_order_2.png}
		\caption{More parts of the tree is modelled.}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.32\textwidth}
		\includegraphics[width=\textwidth]{Pictures/qsm/sphereFollowing/branch_order_3.png}
		\caption{Close to the complete tree is modelled.}
	\end{minipage}
\end{figure}

The algorithm is initialized on a slice at ground height with the thickness \textbf{initialization height}.

Three parameters, namely \textbf{sphere multiplier}, \textbf{sphere epsilon} and \textbf{euclidean clustering distance} can be searched for with the \textbf{auto parameter} search \cite{simpletree_key}. Additionally all three parameters are optimized internally utilizing the downhill simplex method \cite{nelder_key}. Three vectors are choosen which contain percentage numbers. The named parameters are multiplied with the percentage numbers and each potential parameter combination is tested.

The best model is chosen via the cloud to model distance. The parameter set with the smallest cloud to model distance is chosen after parameter optimization. The following options to compute the cloud to model distance are available (\textbf{distance method}):
\begin{itemize}
	\item \textbf{Second Momentum Order} - The distance of each point to the model is squared. This method is the most accurate for perfect point clouds, but also the least robust.
	\item \textbf{Second Momentum Order MSAC} - Same as Second Momentum Order, but here the distance is cropped at a maximum value named \textbf{crop distance}. This allows the algorithm to ignore smaller branches better and gain some robustness.
	\item \textbf{First Momentum Order} - The point distances are not squared and simply summed up. More robust than the above ones.
	\item \textbf{First Momentum Order MSAC} - Same as First Momentum Order, but again the distances are cropped.
	\item \textbf{Zero Momentum Order}. Here the number of inlier points of the input cloud to the model is counted. An inlier is a point closer than \textbf{crop distance}. This method maximises the number of inliers in contrast to the other minimization methods and is the most robust one.
\end{itemize}