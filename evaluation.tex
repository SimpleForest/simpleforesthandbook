\section{Evaluation}\index{Evaluation}
\label{Evaluation}

\subsection{Data}

We use the following data: all clouds, processing and validation scripts, QSMs and result tables available in the \href{https://gitlab.com/SimpleForest/computree/-/tree/master/bin/testScriptsAndData/}{SimpleForest Gitlab repository}:

\begin{itemize}
	\item 12 \textit{Quercus petraea} (leaf off), 12 \textit{Erythrophleum fordii} (leaf on), 12 \textit{Pinus massioniana} (needle on), published in Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key}. The \textit{Quercus petraea} trees have been overgrown with moss.
	\item  65 trees in total of the species \textit{Fraxinus excelsior},  \textit{Fagus sylvatica}, \textit{Pinus sylvestris} and \textit{Larix decidua}, published in Demol \textit{et al.} \textbf{2021} \cite{demol_key}. The \textit{Pinus sylvestris} trees have been scanned in needle condition on, the other species were scanned without foliage.
	\item  29 tropical buttresses trees across sites in Peru, Indonesia and Guyana, published in de Tanago \textit{et al.} \textbf{2017} \cite{gonzales_key}. All trees have been scanned in leaf on condition.
	\item  A plot in leaf off provided by the Leipzig Canopy Crane facility financed by the German Centre for Integrative Biodiversity Research (iDiv) Halle-Jena-Leipzig and the instute of Systematic Botany and Functional Biodiversity, Institute for Biology, Leipzig University. The plot does not have ground truth data in addition, but the accuracy of tree segmentation as well as DTM and QSM modelling can be reviewed visually.
	\item  An \textit{Acer pseudoplatanus} tree cloud, published in Disney \textit{et al.} \textbf{2018} \cite{disney_key}. The scanned individual is very complex and we use it to show some statistical analysis potential.
\end{itemize}

\subsection{Results}

We provide a summary of the results in table \ref{resultTab2}.


\begin{table}[h!]
	\begin{center}
		\begin{tabular}{||c c c r r r r||}
			\hline
			Plot                                                                & Software               & cropped$^1$ & $RMSE$ & $RMSE_{rel.}$ & $CCC$ & $r^{2}_{adj.}$ \\ [0.5ex]
			\hline\hline
			Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key} & SimpleForest $^{a, b}$ &
			$x$                                                                 & 249.84                 & 0.16        & 0.79   & 0.83                                   \\
			\hline
			Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key} & SimpleForest $^{a, b}$ &
			\checkmark                                                          & 208.31                 & 0.13        & 0.84   & 0.89                                   \\
			\hline
			Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key} & SimpleForest $^{c}$ &
			$x$                                                                 & 127.41                 & 0.10        & 0.91   & 0.87                                   \\
			\hline
			Demol \textit{et al.} \textbf{2021} \cite{demol_key}                & SimpleForest $^b$      &
			$x$                                                                 & 385.93                 & 0.10        & 0.83   & 0.86                                   \\
			\hline
			Demol \textit{et al.} \textbf{2021} \cite{demol_key}                & SimpleForest $^b$      &
			\checkmark                                                          & 189.74                 & 0.06        & 0.94   & 0.91                                   \\
			\hline
			Demol \textit{et al.} \textbf{2021} \cite{demol_key}                & TreeQSM                &
			$x$                                                                 & 323.72                 & 0.11        & 0.86   & 0.85                                   \\
			\hline
			Demol \textit{et al.} \textbf{2021} \cite{demol_key}                & TreeQSM                &
			\checkmark                                                          & 205.65                 & 0.19        & 0.92   & 0.96                                   \\
			\hline
			Demol \textit{et al.} \textbf{2021} \cite{demol_key}                & SimpleForest$^{2, b}$  &
			\checkmark                                                          & 159.02                 & 0.08        & 0.94   & 0.98
			\\
			\hline
			de Tanago \textit{et al.} \textbf{2017} \cite{gonzales_key}         & SimpleForest $^a$      &
			\checkmark                                                          & 3483.07                & 0.09        & 0.94   & 0.92                                   \\
			\hline
			combined data$^3$                                                   & SimpleForest$^{a, b}$  &
			\checkmark                                                          & 1383.85                & 0.04        & 0.97   & 0.96                                   \\
			\hline
			Demol \textit{et al.} \textbf{2021} \cite{demol_key}                & SimpleForest$^c$       &
			$x$                                                                 & 140.49                 & 0.06        & 0.97   & 0.93                                   \\
			\hline
			Demol \textit{et al.} \textbf{2021} \cite{demol_key}                & SimpleForest$^c$       &
			\checkmark                                                          & 94.26                  & 0.04        & 0.98   & 0.98                                   \\
			\hline
			Demol \textit{et al.} \textbf{2021} \cite{demol_key}                & SimpleForest$^{2, c}$  &
			$x$                                                                 & 37.25                  & 0.02        & 1.00   & 0.99                                   \\
			\hline
			Demol \textit{et al.} \textbf{2021} \cite{demol_key}                & SimpleForest$^{2, c}$  &
			\checkmark                                                          & 76.42                  & 0.05        & 0.99   & 0.98                                   \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Numerical validation of QSM volume measures compared to harvested reference volume measures.}
	\label{resultTab2}
\end{table}

$1 = $ Cropped means that only biomass larger 7 cm diameter are taken into account. For de Tanago \textit{et al.} \textbf{2017} \cite{gonzales_key} this threshold is enlarged to 10 cm.

$2 = $ Fitered by taking only QSMs close to results produced with TreeQSM.

$3 = $ Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key}, Demol \textit{et al.} \textbf{2021} \cite{demol_key} and de Tanago \textit{et al.} \textbf{2017} \cite{gonzales_key} data.


$a = $ QSM Allometric Correction \ref{QSM Allometric Correction} filtering with the Vesselvolume approach \ref{Vesselvolume}.

$b = $ QSM Allometric Correction \ref{QSM Allometric Correction} filtering with the Tapering approach \ref{Tapering}.

$c = $ QSM Reverse Pipe Model Filter \ref{QSM Reverse Pipe Model Filter} filtering.

\newpage

\subsubsection{Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key}}

We compare the complete QSMs in figures \ref{hacken011a}, \ref{hacken012a}, \ref{hacken013a}

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.8\textwidth}
		\includegraphics[width=\textwidth]{Pictures/evaluation/ery.png}
	\end{minipage}
	\caption{Hackenberg \textit{et al.} \textbf{2015a}
		\cite{hackenberg2015_key} \textit{Erythrophleum fordii} QSMs.}
	\label{hacken011a}
\end{figure}

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.8\textwidth}
		\includegraphics[width=\textwidth]{Pictures/evaluation/oak.png}
	\end{minipage}
	\caption{Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key} \textit{Quercus petraea} QSMs.}
	\label{hacken012a}
\end{figure}

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.8\textwidth}
		\includegraphics[width=\textwidth]{Pictures/evaluation/pine.png}
	\end{minipage}
	\caption{Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key} \textit{Quercus petraea} QSMs.}
	\label{hacken013a}
\end{figure}

\newpage

to total harvested data split up by species \ref{hacken01}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_1_1_1a.pdf}
	\caption{Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key} clouds denoised with SimpleForest before modeling. Full QSMs have been used.}
	\label{hacken01}
\end{figure}

as well as a combined data set \ref{hacken02}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_1_1_1b.pdf}
	\caption{Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key} clouds denoised with SimpleForest before modeling. Full QSMs have been used.}
	\label{hacken02}
\end{figure}


\newpage

We also focus on QSM components larger 7cm enlarged with Biomass Expansion Factors, again split by species

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_1_2_1a.pdf}
	\caption{Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key} clouds denoised with SimpleForest before modeling. Solid wood expanded with BEFs.}
	\label{hacken03}
\end{figure}

as well as a combined data set \ref{hacken04}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_1_2_1b.pdf}
	\caption{Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key} clouds denoised with SimpleForest before modeling. Solid wood expanded with BEFs.}
	\label{hacken04}
\end{figure}

\newpage

\subsubsection{Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key} with QSM Reverse Pipe Model Filter \ref{QSM Reverse Pipe Model Filter}}

When we apply the QSM Reverse Pipe Model Filter \ref{QSM Reverse Pipe Model Filter} to the complete volume we receive figure \ref{hacken03c}, split by species,

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_10_1_1a.pdf}
	\caption{Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key} clouds denoised with SimpleForest before modeling. Solid wood expanded with BEFs.}
	\label{hacken03c}
\end{figure}

or as a combined data set \ref{hacken04c}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_10_1_1b.pdf}
	\caption{Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key} clouds denoised with SimpleForest before modeling. Solid wood expanded with BEFs.}
	\label{hacken04c}
\end{figure}

\newpage

\subsubsection{Demol \textit{et al.} \textbf{2021} \cite{demol_key} with Tapering approach \ref{Tapering} with QSM Allometric Correction \ref{QSM Allometric Correction}}


The complete QSMs of 65 trees in total of the species \textit{Fraxinus excelsior},  \textit{Fagus sylvatica}, \textit{Pinus sylvestris} and \textit{Larix decidua} have been compared to ground truth species wise \ref{demol01}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_2_1_1a.pdf}
	\caption{Demol \textit{et al.} \textbf{2021} \cite{demol_key} clouds. Full QSMs have been used.}
	\label{demol01}
\end{figure}

as well as a combined data set \ref{demol02}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_2_1_1b.pdf}
	\caption{Demol \textit{et al.} \textbf{2021} \cite{demol_key} clouds. Full QSMs have been used.}
	\label{demol02}
\end{figure}

\newpage

We also focus on QSM components larger 7cm, again split by species \ref{demol03}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_2_2_1a.pdf}
	\caption{Demol \textit{et al.} \textbf{2021} \cite{demol_key} clouds. Cropped to mechantable wood larger 7 cm.}
	\label{demol03}
\end{figure}

as well as a combined data set \ref{demol04}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_2_2_1b.pdf}
	\caption{Demol \textit{et al.} \textbf{2021} \cite{demol_key} clouds. Cropped to mechantable wood larger 7 cm.}
	\label{demol04}
\end{figure}

\newpage

\subsubsection{Demol \textit{et al.} \textbf{2021} \cite{demol_key} with QSM Reverse Pipe Model Filter \ref{QSM Reverse Pipe Model Filter}}

The complete QSMs of 65 trees in total of the species \textit{Fraxinus excelsior},  \textit{Fagus sylvatica}, \textit{Pinus sylvestris} and \textit{Larix decidua} have been compared to ground truth species wise \ref{demol01b}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_8_1_1a.pdf}
	\caption{Demol \textit{et al.} \textbf{2021} \cite{demol_key} clouds. Full QSMs have been used.}
	\label{demol01b}
\end{figure}

as well as a combined data set \ref{demol02b}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_8_1_1b.pdf}
	\caption{Demol \textit{et al.} \textbf{2021} \cite{demol_key} clouds. Full QSMs have been used.}
	\label{demol02b}
\end{figure}

\newpage

We also focus on QSM components larger 7cm, again split by species \ref{demol03b}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_8_2_1a.pdf}
	\caption{Demol \textit{et al.} \textbf{2021} \cite{demol_key} clouds. Cropped to mechantable wood larger 7 cm.}
	\label{demol03b}
\end{figure}

as well as a combined data set \ref{demol04b}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_8_2_1b.pdf}
	\caption{Demol \textit{et al.} \textbf{2021} \cite{demol_key} clouds. Cropped to mechantable wood larger 7 cm.}
	\label{demol04b}
\end{figure}

\newpage

\subsubsection{de Tanago \textit{et al.} \textbf{2017} \cite{gonzales_key}}

All 29 clouds of buttresses and evergreen trees have been denoised with \emph{Gaussian Mixture Models Fast Point Feature Histogram} \ref{Gaussian Mixture Models Fast Point Feature Histogram}, \emph{Manual Classification of Gaussian Mixture Models} \ref{Manual Classification of Gaussian Mixture Models} and \emph{Deleaving with Belton et al} \ref{Deleaving with Belton et al} steps, see figures \ref{gonz01}, \ref{gonz02}, \ref{gonz03b} \ref{gonz04b}.

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{Pictures/evaluation/guy01.png}
		\caption{A tree cloud from a Guyanese plot scanned in leaf on condition. The evergreen tree has also a buttress root.}
		\label{gonz01}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{Pictures/evaluation/guy02.png}
		\caption{The same cloud, but denoised with the step \emph{Gaussian Mixture Models Fast Point Feature Histogram} \ref{Gaussian Mixture Models Fast Point Feature Histogram}.}
		\label{gonz02}
	\end{minipage}
\end{figure}

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{Pictures/evaluation/md01.png}
		\caption{A tree cloud from a Indonesian plot scanned in leaf on condition. The evergreen tree has also a buttress root.}
		\label{gonz03b}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{Pictures/evaluation/md02.png}
		\caption{The same cloud, but denoised with the step \emph{Gaussian Mixture Models Fast Point Feature Histogram} \ref{Gaussian Mixture Models Fast Point Feature Histogram}.}
		\label{gonz04b}
	\end{minipage}
\end{figure}

While both denoised as well as undenoised clouds have been modeled with QSMs, the lower 5 meters of stem was in both cases modeled with Poisson mesh reconstruction as proposed in Morel \textit{et al.} \textbf{2018} \cite{morel_key} and Khazdan \textit{et al.} \textbf{2006} \cite{kazhdan_key}, see figure \ref{rootPoisson}. The mesh volume and the QSM volume of the tree parts of height larger 5 meter have been summed up in the evaluation.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/root.png}
	\caption{Poisson reconstructed buttress root of a tropical tree, Morel \textit{et al.} \textbf{2018} \cite{morel_key}.}
	\label{rootPoisson}
\end{figure}

The ground truth data only contained branch components with a diameter larger 10 cm. The analysis on the denoised data split up by species can be seen in figure \ref{gonz03}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_3_2_1a.pdf}
	\caption{de Tanago \textit{et al.} \textbf{2017} \cite{gonzales_key} clouds, denoised. Cropped to mechantable wood larger 10 cm.}
	\label{gonz03}
\end{figure}

All species combined are show in figure \ref{gonz04}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_3_2_1b.pdf}
	\caption{de Tanago \textit{et al.} \textbf{2017} \cite{gonzales_key} clouds, denoised. Cropped to mechantable wood larger 10 cm.}
	\label{gonz04}
\end{figure}

Visual inspection of the 29 qsm produced on the undenoised data revealed that 10 models had to be exluded leaving 19 for the validation, per species analysis is shown in figure \ref{gonz05}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_3_2_2a.pdf}
	\caption{de Tanago \textit{et al.} \textbf{2017} \cite{gonzales_key} clouds, denoised. Cropped to mechantable wood larger 10 cm.}
	\label{gonz05}
\end{figure}

All species combined are show in figure \ref{gonz06}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_3_2_2b.pdf}
	\caption{de Tanago \textit{et al.} \textbf{2017} \cite{gonzales_key} clouds, denoised. Cropped to mechantable wood larger 10 cm.}
	\label{gonz06}
\end{figure}

\subsubsection{The combined data}

We combined the data from Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key}, Demol \textit{et al.} \textbf{2021} \cite{demol_key} and the denoised de Tanago \textit{et al.} \textbf{2017} \cite{gonzales_key}. A plot splitting the total data by species can be seen in figure \ref{comb01}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_4_3_1a.pdf}
	\caption{All three previous data sets combined.}
	\label{comb01}
\end{figure}

All species combined are show in figure \ref{comb02}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_4_3_1b.pdf}
	\caption{All three previous data sets combined.}
	\label{comb02}
\end{figure}

\subsubsection{Comparing to TreeQSM \cite{pasi_key, raumonen_key}}

The data base of Demol \textit{et al.} \textbf{2021} \cite{demol_key} contained TreeQSM (Raumonen \textit{et al.} \textbf{2013} \cite{pasi_key}, Raumonen \textit{et al.} \textbf{2015} \cite{raumonen_key}) results. A plot for that data compares the two tools for the complete QSMs can be seen in figure \ref{treeQSM01}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_5_1_1a.pdf}
	\caption{Comparing SimpleForest results with TreeQSM results on complete QSMs of Demol \textit{et al.} \textbf{2021} \cite{demol_key} data.}
	\label{treeQSM01}
\end{figure}

Figure \ref{treeQSM02} shows a similar comparison, but looking at solid wood components with diameter larger 7cm.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_5_2_1a.pdf}
	\caption{Comparing SimpleForest results with TreeQSM results on mrechantable wood component part of QSMs of Demol \textit{et al.} \textbf{2021} \cite{demol_key} data.}
	\label{treeQSM02}
\end{figure}

If SimpleForest (\textbf{SF}) and TreeQSM (\textbf{TQ}) results can be used together by computing the relative error between the two data, $error = abs((SF_{vol} - TQ_{vol})/SF_{vol})$. Following the conclusion that good predictions of both methods have to be close together, while mispredictions presumingly deviate from each other we can filter out wrong QSMs without knowing ground truh by just relying on two public available software tools, see figure \ref{treeQSM03} for data with $error < 10\%$ filter.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_6_1_1b.pdf}
	\caption{SimpleForest results on Demol \textit{et al.} \textbf{2021} \cite{demol_key} data. Errourness QSMs have been filtered out by using additional TreeQSM results as well.}
	\label{treeQSM03}
\end{figure}

The largest accuracy with $CCC$ and $r^2{adj.}$ both of 1.0 can be reached when we apply the QSM Reverse Pipe Model Filter \ref{QSM Reverse Pipe Model Filter} and focus only on the solid wood in figure \ref{treeQSM04}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Pictures/evaluation/eval_9_2_1b.pdf}
	\caption{SimpleForest results on Demol \textit{et al.} \textbf{2021} \cite{demol_key} data. Errourness QSMs have been filtered out by using additional TreeQSM results as well.}
	\label{treeQSM04}
\end{figure}

\newpage

\subsubsection{Disney \textit{et al.} \textbf{2018} \cite{disney_key}}

For the here presented \textit{Acer pseudoplatanus} we have no ground truth data. Validation has to follow by computational metrics as cloud to model distance or visual inspection. The 1 meter DBH sized tree has more than 5km of branch length and is of huge complexity, see figures \ref{wytham01}, \ref{wytham02}.


\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{Pictures/evaluation/wytham/growthVolume.png}
	\caption{An \textit{Acer pseudoplatanus} individual published in Disney \textit{et al.} \textbf{2018} \cite{disney_key}. The QSM is colored by growthVolume on a logarithm scale.}
	\label{wytham01}
\end{figure}


\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{Pictures/evaluation/wytham/reverseBranchOrder.png}
	\caption{An \textit{Acer pseudoplatanus} individual published in Disney \textit{et al.} \textbf{2021} \cite{disney_key}. The QSM is colored by reverse branch order.}
	\label{wytham02}
\end{figure}

Plot location is Wytham Woods which is a protected forest observed by ecologists of the \href{https://www.eci.ox.ac.uk/research/ecosystems/}{Oxford University}. The cloud and the QSM seem to be of high quality and we see here an old free standing tree individual which growth is a lot more driven by genome patterns than oftenly analysed plantation individuals. Leaf off condition allows reliable QSM modeling.

We show exemplary statistical analysis for the here discussed tree. From a traditional perspective, the stem taper is of large relevance and can be seen in figure \ref{wytham03}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Pictures/evaluation/wytham/PlotStemTaper.png}
	\caption{An \textit{Acer pseudoplatanus} individual published in Disney \textit{et al.} \textbf{2021} \cite{disney_key}. We show the stem taper extracted from the QSM.}
	\label{wytham03}
\end{figure}

We can accumulate the volume of branches by binning along the branches' origin \textit{height above DTM}. Obviously the largest peak is the crown base diameter which is quite close to the ground here (1 meter), see figure  \ref{wytham04}. Figure \ref{wytham01} reveals as well the low height fork of the tree.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Pictures/evaluation/wytham/PlotBranchHistogram.png}
	\caption{An \textit{Acer pseudoplatanus} individual published in Disney \textit{et al.} \textbf{2021} \cite{disney_key}. We show the branch histogram extracted from the QSM.}
	\label{wytham04}
\end{figure}

Plotting the \emph{growthVolume} as a function of the radius as proposed in Hackenberg \textit{et al.} \textbf{2015b} \cite{simpletree_key} can be seen in figure \ref{wytham05}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Pictures/evaluation/wytham/PlotGrowthVolume.png}
	\caption{An \textit{Acer pseudoplatanus} individual published in Disney \textit{et al.} \textbf{2021} \cite{disney_key}. We plot the growthVolume versus radii measurements as proposed in Hackenberg \textit{et al.} \textbf{2015b} \cite{simpletree_key}.}
	\label{wytham05}
\end{figure}

Plotting the \emph{growthLength} as a function of the radius can be seen in figure \ref{wytham06}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Pictures/evaluation/wytham/PlotGrowthLength.png}
	\caption{An \textit{Acer pseudoplatanus} individual published in Disney \textit{et al.} \textbf{2021} \cite{disney_key}. We plot the growthLength versus radii measurements.}
	\label{wytham06}
\end{figure}

Plotting the \emph{Vesselvolume} \ref{Vesselvolume} as a function of the radius can be seen in figure \ref{wytham07}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Pictures/evaluation/wytham/PlotVesselVolume.png}
	\caption{An \textit{Acer pseudoplatanus} individual published in Disney \textit{et al.} \textbf{2021} \cite{disney_key}. We plot the vesselVolume \ref{Vesselvolume} versus radii measurements.}
	\label{wytham07}
\end{figure}

Following this hypothesis \ref{The Forest is the Tree} we provide an allometric function for \textit{Acer pseudoplatanus} from one \textit{single} individual in figure \ref{wytham08} with function \ref{eqAllom03}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Pictures/evaluation/wytham/PlotTreeAllometry.png}
	\caption{An \textit{Acer pseudoplatanus} individual published in Disney \textit{et al.} \textbf{2021} \cite{disney_key}. We fit a volume predicting allometric function from a single tree individual.}
	\label{wytham08}
\end{figure}

We can also bin the cylinders' volume by branch order in figure \ref{wytham09}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Pictures/evaluation/wytham/PlotBranchOrderHistogramVolume.png}
	\caption{An \textit{Acer pseudoplatanus} individual published in Disney \textit{et al.} \textbf{2021} \cite{disney_key}. We show a volume histogram binned by branch order.}
	\label{wytham09}
\end{figure}

Or bin the cylinders length amongst the branch order, as shown in figure \ref{wytham10}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Pictures/evaluation/wytham/PlotBranchOrderHistogramLength.png}
	\caption{An \textit{Acer pseudoplatanus} individual published in Disney \textit{et al.} \textbf{2021} \cite{disney_key}. We show a length histogram binned by branch order.}
	\label{wytham10}
\end{figure}

In previous work we showed the capabilities to test older theories as the Pipe Model Theory presented in Shinozaki \textit{et al.} \textbf{1964} \cite{shinozaki_key}, we do so here as well in figure \ref{wytham11}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Pictures/evaluation/wytham/PlotPipeModelTheory.png}
	\caption{An \textit{Acer pseudoplatanus} individual published in Disney \textit{et al.} \textbf{2021} \cite{disney_key}. We test the pipe model theory (Shinozaki \textit{et al.} \textbf{1964}
		\cite{shinozaki_key}), as previously done in Hackenberg \textit{et al.} \textbf{2015b} \cite{simpletree_key}.}
	\label{wytham11}
\end{figure}

\newpage

\subsubsection{High quality plot - Visual inspection only}
\index{high quality plot}
\label{high quality plot}

The plot was processed fully automatically including DTM generation and tree cloud segmentation with a 33 step
\href{https://gitlab.com/SimpleForest/computree/-/blob/master/bin/testScriptsAndData/plotHighResoluted/HighResolutedPlot.xsct2}{Computree script}, which can be downloaded as well as QSMs in csv and ply files for user inspection and reproduction. Coloring the QSMs by their tree ID reveals that the segmentation was working out well, see figure \ref{plotHigh01}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{Pictures/evaluation/QSMTreeId.png}
	\caption{The QSMs are colored randomly by their unique tree ID.}
	\label{plotHigh01}
\end{figure}

Also the DTM has been modelled in a meaningful way. The forest road clearly visible in figure \ref{dtm0001} is modelled accurately in figure \ref{dtm0002}. Extrapolation of outside plot area looks reasonable.

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.48\textwidth}
		\includegraphics[width=\textwidth]{Pictures/dtm/groundPoints.png}
		\caption{The ground points colored by height.}
		\label{dtm0001}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.48\textwidth}
		\includegraphics[width=\textwidth]{Pictures/dtm/dtm.png}
		\caption{The Digital terrain model colored grayscale by height.}
		\label{dtm0002}
	\end{minipage}
\end{figure}

\newpage

For a better visual impression refer of QSM quality refer to figures \ref{plotHigh02}, \ref{plotHigh03}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{Pictures/evaluation/QSMGrowthVolume.png}
	\caption{The \textit{growthVolume} is here color coded.}
	\label{plotHigh02}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{Pictures/evaluation/QSMBranchOrder.png}
	\caption{The \textit{branch order} is here color coded.}
	\label{plotHigh03}
\end{figure}

\newpage

\subsection{Discussion}

A fully automatic pipeline is provided \href{https://gitlab.com/SimpleForest/computree/-/tree/master/bin/testScriptsAndData/plotHighResoluted}{as a download}
which can be used to process TLS forest plot sceneries after adapting parameters to scanning setup and site features. The full automatism of modeling raw plot scans to QSMs can at least be assured for leaf-off conditions, see section \ref{high quality plot}. We as well provide online
\href{https://gitlab.com/SimpleForest/computree/-/tree/master/bin/testScriptsAndData/plotLowResoluted}{cloud data and processing script}
for an example of a single scan and low resolution forest point cloud. All compute QSMs are available in ply polygon data additionally for 3d visualization and inspection with tools like \href{http://cloudcompare.org/}{CloudCompare}.

Numerical evaluation with error measures available in table \ref{resultTab2} on all available open access data reveals in general good quality for the Hackenberg \textit{et al.} \textbf{2015a} \cite{hackenberg2015_key} data. The systematic overestimation of \textit{Quercus petraea} revealed in figures \ref{hacken01}, \ref{hacken02} can be explained by moss growth.

SimpleForest has overall same accuracy to TreeQSM when we compare with de Tanago \textit{et al.} \textbf{2017} \cite{gonzales_key} data shown in figures \ref{gonz03}, \ref{gonz04}, \ref{gonz05} and \ref{gonz06}. The error measures for TreeQSM can be derived directly from de Tanago \textit{et al.} \textbf{2017} \cite{gonzales_key}.

Looking at Demol \textit{et al.} \textbf{2021} \cite{demol_key} data the needle species \textit{Pinus sylvestris}, see figures \ref{demol01}, \ref{demol03} is a lot overestimated. While the denoised input clouds revealed good visual inspected quality there might be still an input data error on needle trees.
Compared to results of TreeQSM we can see that SimpleForest QSMs have a larger overestimation, when we apply the tapering filter. But the QSM Reverse Pipe Model Filter \ref{QSM Reverse Pipe Model Filter} seems to be a lot stronger and SimpleForest outperforms TreeQsm when this filter is applied.

As SimpleForest is able to import TreeQSM clouds for direct visual comparison a lot of insight can be gained on this data set and the difference between the two methods. Using new features for allometric predictions can be seen superior to traditional ones. Statistical plots such as figure \ref{la9} show that relations between new features such as the reverse pipe model branch order have stronger relations. It is recommended to read sections Branchorder Types \ref{Branchorder Types} and Growthparameters \ref{Growthparameters} to gain insight how cylinder features relate more to the supported tips rather than to the root as traditional approaches do.

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{Pictures/definitions/reversePipeRadiusBranchorder.png}
		\caption{We can see a strong linear relation between reverse pipe radius Branchorder and the radius.}
		\label{la9}
	\end{minipage}
\end{figure}

Nevertheless, as both tools are open source one can filter by requesting  the a ratio of the two QSMs' volumina to be close to 1. Results seen in figure \ref{treeQSM04} are of highest quality with $RMSE_{rel.} = 0.02$, $CCC = 1.00$ and $r^2_{adj.} = 0.99$.